"""
This python file collects all the messages from the other imported message files
in this directory and builds them into a dictionary.

This dictionary allows other functions to access the relevant funciton by
entering top category and topic.
"""
#Imports
import goal_messages
import stage_messages
import issue_messages
import local_messages
import inspect


def make_sub_dict(package):
    """
    package:
        (imported python package), the imported python package to turn into a
        dictionary

    This function makes a package into a dictioanry. Each funciton name acts as
    a key in the dictionary, which queues up the actual function.

    it selects only functions that have 'message' in their name, and filters out
    functions with the names in the 'strips' variable.

    Returns dict, in this format:
        {
        'function_name1' : function1,
        'function_name2' : function2,
        ...
        }
    """
    strips = ['generate_message', 'make_message']
    funcs = inspect.getmembers(package, inspect.isfunction)
    fdict = {k : v for k, v in funcs if 'message' in k and k not in strips}

    return fdict

#Building category-wise dictionary of functions
msg_dict = {
    'issue' : make_sub_dict(issue_messages),
    'local' : make_sub_dict(local_messages),
    'stage' : make_sub_dict(stage_messages),
    'goal' : make_sub_dict(goal_messages)
    }
