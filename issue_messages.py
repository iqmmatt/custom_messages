import pandas as pd
from collections import Counter
import urllib
from message_creation import *
import requests as rq
from bs4 import BeautifulSoup

states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}

idf = pd.read_csv('./with_score_key.csv')

diff_and_loc = pd.read_csv('./2007_2016_weather.csv')

"""
UTILITY SECTION
"""

def get_state(lat, lon):
    """
    lat:
        (float), the lattitude of the incoming request
    lon:
        (float), the longitute of the incoming request

    This function cross-references the user's lat and lon with our weather
    station data to guess at what state the user is in. The funciton finds the
    nearest weather station to the user, and returns that station's state.

    This funciton is a convenient stop-gap solution, and will be replaced with a
    better function in the future

    Returns string, the predicted state
    """
    #Ensuring lat and lon are floats
    lat, lon = float(lat), float(lon)

    #Calculating distances
    tdf = diff_and_loc[:]
    tdf['dist'] = (lat - tdf['Latitude'])**2 + (lon - tdf['Longitude'])**2
    r = tdf.sort_values('dist').iloc[0:1]

    #Getting state from closest station
    state = r['State'].iloc[0]
    state = states[state]

    return state

def make_message(lat, lon, years, voter_vector, func_key):
    """
    lat:
        (float), the lattitude of the incoming request
    lon:
        (float), the longitute of the incoming request
    years:
        (int or float, 2, 4 or 6), the years of the targeted elected office
    voter_vector:
        (array), the voter's HayStaq score vector on issues
    func_key:
        (str), the dictionary key for the funciton to use

    This funciton calls the main push_sent funciton and the function indicated
    by te func_key to return two strings

    Returns two strings
    """

    func = func_dict[func_key]

    fact = func(lat, lon)
    push_sent = make_push_sent(years, voter_vector)

    return push_sent, fact

"""
CLIMATE MESSAGES
"""

def climate_message(years):
    voter_vector = np.random.rand(100, 1)
    #Trimming the issues DF to only those things we care about
    print(idf.head())
    cutidf = idf[idf['match'].isin(['climate_change_believer_score'])]

    #Getting the indicies of the issues we care about
    non_zeros = [int(x) for x in cutidf.index]

    #Zeroing out the values for non-targeted issues
    np.put(
        voter_vector,
        [x for x in range(len(voter_vector)) if x not in non_zeros],
        [0]
        )
    push_sent = make_push_sent(years, voter_vector)
    img = 'https://davidsuzuki.org/wp-content/uploads/2017/10/Climate-change-basics-what-is-climate-change-hero-1024x615.jpg'
    return push_sent, img

"""
MARIJUANA MESSAGES
"""

def get_latest_marijuana_data():
    """
    This function scrapes the most recent data on marijuana legalization among
    us states from wikipdia

    Returns a dataframe
    """

    headers = ['state', 'blank', 'recreational', 'medical', 'transportation',
                'cultivation', 'notes']

    url = 'https://en.wikipedia.org/wiki/Legality_of_cannabis_by_U.S._jurisdiction'
    r = rq.get(url)
    souped = BeautifulSoup(r.content, 'html.parser')

    #Establishing empty dataframe
    combined = pd.DataFrame()

    #setting counter (used for assigning the correct region)
    k = 0

    #iterating through wikitables
    for table in souped.findAll('table', {'class' : 'wikitable sortable'}):

        #initiating empty list
        rows = []

        #iterating through table rows
        for tr in table.findAll('tr'):
            #getting text
            row = [x.text.strip() for x in tr.findAll('td')]
            rows.append(row)

        #making dataframe
        tdf = pd.DataFrame(rows)
        tdf.columns = headers
        tdf = tdf.drop('blank', axis=1)

        #concatenating dataframe
        combined = pd.concat([combined, tdf])
        combined.dropna(subset=['state'], inplace=True)
        combined = combined[:51]

    return combined

marijuana_data = get_latest_marijuana_data()
marijuana_data = marijuana_data.set_index('state')

def marijuana_message(years):
    voter_vector = np.random.rand(100, 1)
    #Trimming the issues DF to only those things we care about
    cutidf = idf[idf['match'].isin(['marijuana_legalization_support_score'])]

    #Getting the indicies of the issues we care about
    non_zeros = [int(x) for x in cutidf.index]

    #Zeroing out the values for non-targeted issues
    np.put(
        voter_vector,
        [x for x in range(len(voter_vector)) if x not in non_zeros],
        [0]
        )
    push_sent = make_push_sent(years, voter_vector)
    img = 'https://hightimes.com/wp-content/uploads/2017/10/Misleading-Campaign-Marijuana-Legalization-New-York-1.jpg'
    return push_sent, img
