"""
This file does the bulk of the work for this module. It takes the data from the
front end, finds the appropriate functions from the back end to generate the
appropriate message and then hands the data back to the front end for population.
"""
# imports
from message_repo import msg_dict
import inspect
import pandas as pd


idf = pd.read_csv('./with_score_key.csv')

def available_topics():
    """
    ||Currently Depricated||
    A function to extract the available topic functions from the idf dataframe
    """

    tdf = idf.dropna(subset=['function'])['match']
    issue_list = [x for x in tdf]

    return issue_list

def generate_ad(lat, lon, years, voter_vector,
    size = (320, 250),
    issues = [
        'marijuana_legalization_support_score',
        'climate_change_believer_score'
            ],
    ):
    """
    ||Depricated||
    lat:
        (float), the lattitude for the targeted ad
    lon:
        (float), the longitude for the targeted ad
    years:
        (int or float, 2, 4 or 6), the years of the targeted elected office
    voter_vector:
        (array), the voter's HayStaq score vector on issues
    size:
        (tuple of ints), the size of the ad to return
    issues:
        (list of str), the list of positions that the candidate wants to promote

    This function does the following:
        1.  Redeuces everything in the voter vector to zero if that portion of
            the vector doesn't line up with a targeted issue
        2.  Finds the issue (of the remaining issues) that the voter is most
            strongly aligned with
        3.  Uses that data to identify the correct background image and function
            to use for the ad
        4.  Hands the data off to the identified function
        5.  Gets message data from the identified funciton
        6.  Creates an html string with the correct data dropped in

    Returns string, the html for the dynamic ad
    """
    vector = voter_vector.copy()

    #Trimming the issues DF to only those things we care about
    cutidf = idf[idf['match'].isin(issues)]

    #Getting the indicies of the issues we care about
    non_zeros = [int(x) for x in cutidf.index]

    #Zeroing out the values for non-targeted issues
    np.put(
        vector,
        [x for x in range(len(voter_vector)) if x not in non_zeros],
        [0]
        )

    #Getting the funciton to use to generate the messaging
    func_key = idf.loc[vector.argmax()]['function']

    #Extracting the image to use in the function
    img = idf.loc[vector.argmax()]['img']

    #Getting push and fact sentences
    push, fact = make_message(lat, lon, years, vector, func_key)
    template_file = 'templates/template.html'

    with open(template_file, 'r') as f:
        template = f.read()

    #Creating HTML string
    output = template.format(
        str(size[0]),
        str(size[1]),
        push,
        fact,
        img)

    return output


def generate_multiple_ads(lat, lon, years, voter_vector,
    size = (320, 250),
    issues = [
        'climate_change_believer_score',
        'marijuana_legalization_support_score',
            ]):
    """
    ||Depricated||

    lat:
        (float), the lattitude for the targeted ad
    lon:
        (float), the longitude for the targeted ad
    years:
        (int or float, 2, 4 or 6), the years of the targeted elected office
    voter_vector:
        (array), the voter's HayStaq score vector on issues
    size:
        (tuple of ints), the size of the ad to return
    issues:
        (list of str), the list of positions that the candidate wants to promote

    This function does the following:
        1.  Redeuces everything in the voter vector to zero if that portion of
            the vector doesn't line up with a targeted issue
        2.  Finds the issue (of the remaining issues) that the voter is most
            strongly aligned with
        3.  Uses that data to identify the correct background image and function
            to use for the ad
        4.  Hands the data off to the identified function
        5.  Gets message data from the identified funciton
        6.  Creates an html string with the correct data dropped in

    Returns string, the html for the dynamic ad
    """

    outputs = []
    for issue in issues:
        issue = [issue]

        output = generate_ad(lat, lon, years, voter_vector, size, issue)

        outputs.append(output)

    return outputs

def generate_json_params(lat, lon, years, voter_vector,
    size = (320, 250),
    issues = [
        'marijuana_legalization_support_score',
        'climate_change_believer_score'
            ],
    ):
    """
    ||Depricated||
    lat:
        (float), the lattitude for the targeted ad
    lon:
        (float), the longitude for the targeted ad
    years:
        (int or float, 2, 4 or 6), the years of the targeted elected office
    voter_vector:
        (array), the voter's HayStaq score vector on issues
    size:
        (tuple of ints), the size of the ad to return
    issues:
        (list of str), the list of positions that the candidate wants to promote

    This function does the following:
        1.  Redeuces everything in the voter vector to zero if that portion of
            the vector doesn't line up with a targeted issue
        2.  Finds the issue (of the remaining issues) that the voter is most
            strongly aligned with
        3.  Uses that data to identify the correct background image and function
            to use for the ad
        4.  Hands the data off to the identified function
        5.  Gets message data from the identified funciton
        6.  Creates an html string with the correct data dropped in

    Returns string, the html for the dynamic ad
    """
    vector = voter_vector.copy()

    #Trimming the issues DF to only those things we care about
    cutidf = idf[idf['match'].isin(issues)]

    #Getting the indicies of the issues we care about
    non_zeros = [int(x) for x in cutidf.index]

    #Zeroing out the values for non-targeted issues
    np.put(
        vector,
        [x for x in range(len(voter_vector)) if x not in non_zeros],
        [0]
        )

    #Getting the funciton to use to generate the messaging
    func_key = idf.loc[vector.argmax()]['function']

    #Extracting the image to use in the function
    img = idf.loc[vector.argmax()]['img']

    #Getting push and fact sentences
    push, fact = make_message(lat, lon, years, vector, func_key)

    param = {
        "height" : str(size[0]),
        "width" : str(size[1]),
        "head" : push,
        "tail" : fact,
        "img_url" : img,
        }

    return param

def make_json2(data, head_topic, tail_topic):
    """
    data:
        (ImmutableDict), form data passed from the front end
    head_topic:
        (str), the topic that the user chose to have on the top of their sample
        ad
    tail_topic:
        (str), the topic that the user chose to have on the bottom of their
        sample ad

    This function does the following:
        1. Gets a message and from message_getter for both head and tail
        2. Populates a JSON with the results from message_getter

    Returns dict
    """
    slots = ['head_topic', 'tail_topic']

    texts = []
    imgs = [] #Only the first will be kept

    #Running message_gtter
    for x, y in zip(slots, [head_topic, tail_topic]):
        if x in data:
            txt, img = message_getter(data[x], y, data)
            texts.append(txt)
            imgs.append(img)
        else:
            texts.append('')
            imgs.append('')

    #Building dict
    param = {
        "height" : str(data['height']),
        "width" : str(data['width']),
        "head" : texts[0],
        "tail" : texts[1],
        "img_url" : imgs[0],
        "committee_name" : data['committee_name'],
        }

    return param

def make_multiple_jsons2(data):
    """
    data:
        (ImmutableDict), form data passed from the front end

    This funciton does the following:
        1. Extracts two lists, head_topics and tail_topics, from the form data
        2. Extracts the topic categories from the front end
        3. Runs make_json2 once for each combination of head and tail
        4. Compiles a list of dictionaries and sends them to the front end

    Returns list of dicts
    """
    #Getting categories
    head_cat = data.get('head_topic')
    tail_cat = data.get('tail_topic')

    #Extracting lists of topics
    hts = data.getlist(head_cat)
    tts = data.getlist(tail_cat)

    params = []

    #Iterating through combinations
    for head_topic in hts:
        for tail_topic in tts:
            param = make_json2(data, head_topic, tail_topic)
            params.append(param)

    return params

def message_getter(topic, key, kwargs):
    """
    topic:
        (str), the high level topic that a function belongs to. This correlates
        to which of the '_messages' files the function resides in
    key:
        (str), the specific name of the function to call
    kwargs:
        (dict), the information entered by the user on the front-end

    This function does the following:
        1. Finds the specific message-generating function the user asked for
           through passing keys into two levels of dictionaries
        2. Finds the names of all variables that the specific functions needs
        3. Extracts those variables from the user entries
        4. Passes the variables into the function
        5. Generates a message string

    Returns tuple of two strings, a message string and an image url
    """
    fdict = msg_dict[topic] #Getting the dictionary of functions from a file
    func = fdict[key] #Getting the specific function
    args = inspect.getfullargspec(func)[0] #Finding the args the function seeks
    vars = [kwargs[a] for a in args] #Extracting the variables

    return func(*vars) #Returning tuple


def generate_multiple_jsons(lat, lon, years, voter_vector,
    size = (320, 250),
    issues = [
        'climate_change_believer_score',
        'marijuana_legalization_support_score',
            ]):
    """
    ||Depricated||
    """

    outputs = []
    k = 0
    for issue in issues:
        issue = [issue]

        output = generate_json_params(lat, lon, years, voter_vector, size, issue)
        output['index'] = str(k)

        outputs.append(output)
        k += 1

    return outputs

def update_jsons(params):
    """
    params:
        (list of dict), the data passed from the front-end

    This function does the following:
        1. Iterates through the dicts sent from the front-end
        2. Looks for items in the dict with:
            a. A key that includes 'new'
            b. A value that isn't an empty string
        3. Copies 'new' values over the existing values of the same name
        4. Removes all 'new' keys from the dictionaries
        5. Returns a list of altered dicts

    Returns list of dicts
    """
    new_keys = [x for x in params[0].keys() if 'new' in x]
    replace = {k : k.replace('new_', '') for k in new_keys}

    for param in params:

        for r in replace:

            if param[r] != '':

                param[replace[r]] = param[r]

            param.pop(r)

    return params

def save_creative(param, folder):

    print('save creative')

    template_file = 'templates/template.html'

    with open(template_file, 'r') as f:
        template = f.read()

    output = template.format(
        param['height'],
        param['width'],
        param['head'],
        param['tail'],
        param['img_url'])

    name = '{}x{}_{}_{}.html'
    name = name.format(
                param['height'],
                param['width'],
                param['head'].split(' ')[-1],
                param['tail'].split(' ')[-1])

    with open('{}/{}'.format(folder, name), 'w') as f:
        f.write(output)
