import pandas as pd
from collections import Counter
import urllib
from message_creation import *
import requests as rq
from bs4 import BeautifulSoup

headers = ['state', 'blank', 'recreational', 'medical', 'transportation',
            'cultivation', 'notes']

states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}

loc_df = pd.read_csv('2007_2016_weather.csv')

def get_latest_marijuana_data():

    url = 'https://en.wikipedia.org/wiki/Legality_of_cannabis_by_U.S._jurisdiction'
    r = rq.get(url)
    souped = BeautifulSoup(r.content, 'html.parser')

    #Establishing empty dataframe
    combined = pd.DataFrame()

    #setting counter (used for assigning the correct region)
    k = 0

    #iterating through wikitables
    for table in souped.findAll('table', {'class' : 'wikitable sortable'}):

        #initiating empty list
        rows = []

        #iterating through table rows
        for tr in table.findAll('tr'):
            #getting text
            row = [x.text.strip() for x in tr.findAll('td')]
            rows.append(row)

        #making dataframe
        tdf = pd.DataFrame(rows)
        tdf.columns = headers
        tdf = tdf.drop('blank', axis=1)

        #concatenating dataframe
        combined = pd.concat([combined, tdf])
        combined.dropna(subset=['state'], inplace=True)
        combined = combined[:51]

    return combined

marijuana_data = get_latest_marijuana_data()
marijuana_data = marijuana_data.set_index('state')

def marijuana_fact(lat, lon):

    lat, lon = float(lat), float(lon)
    tdf = loc_df[:]
    tdf['dist'] = (lat - tdf['Latitude'])**2 + (lon - tdf['Longitude'])**2
    r = tdf.sort_values('dist').iloc[0:1]
    state = r['State'].iloc[0]
    state = states[state]

    sent = "Marijuana is {} in {}."

    facts = marijuana_data.loc[state]
    rec = facts['recreational']
    med = facts['medical']

    legals = ['felony', 'misdemeanor']

    if rec == med:
        return sent.format(rec, state)

    elif any([x in rec for x in legals]):
        for x in legals:
            if x in rec:
                return sent.format('a ' + x, state)

    else:
        return sent.format('partially legal', state)


def marijuana_message(lat, lon, years, voter_vector):
    climate_fact = marijuana_fact(lat, lon)
    push_sent = make_push_sent(years, voter_vector)

    return push_sent, climate_fact
