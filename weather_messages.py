import pandas as pd
from collections import Counter
import urllib
from message_creation import *

diff_and_loc = pd.read_csv('./2007_2016_weather.csv')

def get_climate_fact(lat, lon):

    lat, lon = float(lat), float(lon)

    style_dict = {'AvgMaxTemp' : "Average maximum temperatures",
                  'AvgMinTemp' : "Average minimum temperatures",
                  'AvgTemp' : 'Average temperatures',
                  'TotalMonthlyPrecip' : 'Precipitation'}

    base_string = "{} in {} up {}% since 2007."

    tdf = diff_and_loc[:]
    tdf['dist'] = (lat - tdf['Latitude'])**2 + (lon - tdf['Longitude'])**2
    r = tdf.sort_values('dist').iloc[0:1]
    loc = r['Name'].iloc[0].title()
    tcol = r[['AvgMaxTemp', 'AvgMinTemp', 'AvgTemp', 'TotalMonthlyPrecip']].idxmax(axis=1)
    num = r[tcol].iloc[0][0] * 100
    tcol = tcol.iloc[0]
    num = str(round(num, 1))

    return base_string.format(style_dict[tcol], loc, num)


def climate_message(lat, lon, years, voter_vector):
    climate_fact = get_climate_fact(lat, lon)
    push_sent = make_push_sent(years, voter_vector)

    # return ''.join([push_sent, climate_fact])
    return push_sent, climate_fact

vlen = 60
lat = 20
lon = -90

# vector

for i in range(31, 32):
    vector = pd.np.zeros((vlen, 1))
    vector[i] = 1
    print(i)
    print(climate_message(lat, lon, 4, vector))
