"""
STAGE-BASED MESSAGES

All functions in this file should return a tuple with two strings. The first
should be a message of some sort. The second should be a url to an image.

The intermediate mechanism between calling the function and the output can be as
complicated as you like, but it must return a tuple of two strings.

If you add a new function with novel inputs, those inputs should be mirrored on
the module's front-end.

All functions included in this file with 'message' in their name will be
auto-populated to this module's front-end.
"""
def primary_message(candidate, date):
    msg = "Vote for {} in the primary on {}".format(candidate, date)
    img = 'http://03594f6.netsolhost.com/WordPress/wp-content/uploads/2012/04/red-and-blue-make-black-med.jpg'
    return msg, img


def general_message(candidate, date):
    msg = "Vote for {} on {}".format(candidate, date)
    img = 'https://www.xmple.com/wallpaper/red-linear-gradient-blue-1920x1080-c2-ff0000-00008b-a-105-f-14.svg'
    return msg, img


def debate_message(candidate, date):
    msg = "Watch {} in the debate {}".format(candidate, date)
    img = "http://paharulgol.com/wp-content/uploads/black-and-blue-abstract-art-abstract-in-red-blue-black-painting-joe-michelli.jpg"
    return msg, img
