This module generates custom messages for voters based on their most strongly
held view according to their HaystaqDNA voter model.

When loaded, the message_creation.py module extracts custom_message_fragments.csv
into a dataframe, which gives us the custom sentence fragments.

generate_message(voter_vector, years, ed) returns a string that says something
to the effect of "Do you want four more years of men in girls' bathrooms? Vote
Saturday, October 6." The datestamp, the years relevant to the election (four,
in this case), the issue fragment ("men in girls' bathrooms") and the vote push
("Vote..." in this case) will all change based on the inputs.

TODO:

1.  We will need a layer that pre-filters the voter-vectors based on the
    issues the campaign wants to focus on.

      1a. We likely also want a pipeline to allow campaigns to customize the
          sentence fragments for their chosen issues.

2.  The order of the headers in the DataFrame and the voter vector file do
    not match, and likely won't match. We will have to refactor to use the
    vector to get the softmax header from the original file and use that to
    extract the sentence fragment from DataFrame.

3.  We need a way to pull in raw voter vectors.

4.  message_creation.py is presently programmed with the assumption that
    different voters will be motivated by different voting language ("vote" /
    "be a voter") and that we will have data to determine that. We either need
    a way to bring in that data, or we need to remove this portion of the code.
