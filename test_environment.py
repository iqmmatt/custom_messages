from flask import Flask, render_template, request, g
import numpy as np
from voter_to_html import *
from collections import OrderedDict

app = Flask(__name__)

sample_df = pd.DataFrame()

@app.route('/old', methods=['GET', 'POST'])
def make_message():
    """
    This function extracts the following variables from a get request:
        * lat (lattitude)
        * lon (longitude)
        * years (number of years for the elected position)
        * height (pixel height of the ad)
        * width (pixel width of the ad)

    Using that information, it returns a page with an embedded ad made from
    those inputs.

    Broadly, the process works like this:
        1. Extract variables
        2. Finds the voter vector in the dataframe
            (This currently is not being used)
        3. If it can't find the voter vector, it creates a random one
            (This is currently the only way it's handling voter vectors)
        4. Hands the vector and variables off to generate_ad
        5. Returns the page with the generated ad embedded

    At present, the process on this flow is far from perfect, but this is a
    proof of concept
    """

    topics = available_topics()

    keys = [
        'lat',
        'lon',
        'years',
        'height',
        'width',
        ]

    data = request.args
    issues = [x for x in data if x not in keys + ['iqmid']]


    dkeys = [x for x in data.keys()]

    #cleaning the data
    vars = [float(data[k]) for k in keys if k in dkeys]

    iqmid = data.get('iqmid', 0)

    #handling iqmid
    if iqmid in sample_df.index:
        vector = sample_df.loc[iqmid]
    else:
        vector = np.random.rand(100, 1)

    print(issues)
    #Getting ad, but ONLY if all the variables are present
    if len(vars) == 5:
        # ad = generate_ad(*vars[:3], vector, size=(vars[-2], vars[-1]))
        ads = generate_multiple_ads(*vars[:3], vector, size=(vars[-2], vars[-1]),
                                    issues = issues)
    else:
        ad = 'placeholder for ad'
        ads = ['placeholder for ad']

    # print(ad)

    return render_template(
                        'main.html',
                        ads = ads,
                        vars = vars,
                        iqmid = iqmid,
                        topics = topics,
                        issues = issues,
        )

@app.route('/', methods=['POST', 'GET'])
def ads_from_json():
    """
    This function populates the main page. It dynamically populates options for
    the user based on functions in four other files:
        * stage_messages
        * local_messages
        * issue_messages
        * goal_messages

    When given selections through the front-end interface, it will populate one
    to several sample advertisements based on the inputs.
    """
    # data = request.args
    data = request.form
    params = make_multiple_jsons2(data)
    fields = ['height', 'width', 'lat', 'lon', 'years', 'candidate', 'date',
            'committee_name']
    data2 = OrderedDict()
    for k in fields:
        data2[k] = data.get(k, '')
    data = data2
    
    return render_template(
                        'update.html',
                        params = params,
                        topics = msg_dict,
                        data = data,
        )


@app.route('/new_message', methods=['GET', 'POST'])
def updated_message():
    """
    This function allows the user to change one or more sample ads at a time by
    using the attached text entry boxes.

    The function checks entries in each box. If it finds one, it overrides the
    existing text.
    """

    data = request.form
    new_heads = [x for x in data.getlist('new_head')]
    params = []

    for i in range(len(new_heads)):
        param = {}
        for k in request.form.keys():
            param[k] = data.getlist(k)[i]
        params.append(param)

    params = update_jsons(params)

    fields = ['height', 'width', 'lat', 'lon', 'years', 'candidate', 'date',
        'committee_name']
    data2 = OrderedDict()
    for k in fields:
        data2[k] = data.get(k, '')
    data = data2

    return render_template(
                            'update.html',
                            params = params,
                            topics = msg_dict,
                            data = data
                            )

@app.route('/save', methods=['GET', 'POST'])
def save():
    """
    This function allows the user to change one or more sample ads at a time by
    using the attached text entry boxes.

    The function checks entries in each box. If it finds one, it overrides the
    existing text.
    """

    data = request.form
    print(data)

    save_creative(data, 'default')
    # new_heads = [x for x in data.getlist('new_head')]
    # params = []
    #
    # for i in range(len(new_heads)):
    #     param = {}
    #     for k in request.form.keys():
    #         param[k] = data.getlist(k)[i]
    #     params.append(param)
    #
    # params = update_jsons(params)
    #
    # return render_template(
    #                         'update.html',
    #                         params = params,
    #                         topics = msg_dict,
    #                         )

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
