"""
GOAL-BASED MESSAGES

All functions in this file should return a tuple with two strings. The first
should be a message of some sort. The second should be a url to an image.

The intermediate mechanism between calling the function and the output can be as
complicated as you like, but it must return a tuple of two strings.

If you add a new function with novel inputs, those inputs should be mirrored on
the module's front-end.

All functions included in this file with 'message' in their name will be
auto-populated to this module's front-end.
"""

def vote_message(candidate, date):
    msg = "Vote for {} on {}".format(candidate, date)
    img = 'https://www.setaswall.com/wp-content/uploads/2017/12/Blue-And-Black-Wallpaper-08-1920x1080.jpg'
    return msg, img

def donate_message():
    msg = "Donate today"
    img = 'https://images.fineartamerica.com/images-medium-large/blue-and-red-pastel-conversation-kazuya-akimoto.jpg'
    return msg, img

# def new_message():
#     msg = "NEW MESSAGE"
#     img = 'https://images.fineartamerica.com/images-medium-large/blue-and-red-pastel-conversation-kazuya-akimoto.jpg'
#     return msg, img
